# NOTE:  if reproduceable_builder, xml_builder, and uml_models are switched from 'require' to 'load' a stack overflow occurs on the second execution of xml schema generation.
#        The resulting stack dump is inconsistent, often does not show much depth, and tends to occur in innocuous code.
#        I've not investigated which of these files is actually responsible, or why they behave this way.


# puts "
# CAUTION
#
# extensions previously had circular dependency on schemaGen.
# 'schemaGen/xml_builder' is thought to not be required here. Removed it as part of breaking circular dependency
#
# If something goes wrong, move a minimal portion of 'schemaGen/xml_builder' into magicdraw_extensions!!
# "
#
# require 'schemaGen/xml_builder'
#
#
#

load 'magicdraw_extensions.rb'
require 'modelDriven_builder/ruby_extensions'  # Cannot load more than once because of "alias broken_compare <=>"
load 'modelDriven_builder/meta_info.rb'
load 'modelDriven_builder/helper.rb'
load 'modelDriven_builder/reproduceable_builder.rb'
load 'modelDriven_builder/director.rb'
load 'modelDriven_builder/builder.rb'