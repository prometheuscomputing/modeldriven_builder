require 'modelDriven_builder/topo_sorter'

# Operates as the builder for ModelDriven_Director.
# This provides some useful default methods, but it is abstract, and sublclasses need to re-implement at least some of the methods.
class ModelDriven_Builder
  
  attr_accessor :perform_topo_sorts # A boolean. Defaults to nil which acts like false.
  
  # Lazy initialized (for backwards compatability)
  def package_stack; @package_stack||=Array.new; end
  
  # A lazy initialized Array of strings
  def problems; @problems||=Array.new; end
  def result
    return "Successful" if problems.empty?
    problems.join("\n")
  end
  
  def clear; problems.clear; package_stack.clear; Warnings.clear; end
  
  # Queries =============
  
  # These methods return a modified version of the argument. 
  # They can reorder the argument, and/or drop items from the argument. They cannot add new values to the argument.
  
  def pkg_content_sequence(cats); cats; end
  def class_cat_sequence(cats); cats; end
  alias interface_cat_sequence class_cat_sequence
  alias datatype_cat_sequence class_cat_sequence
  
  # If this returns true, attributes and associations for the superclass will be provided to process_class_X and process_interface_X
  # Normally false. Useful for "flattening" hierarchies (for example, ASN.1 definitions)
  def pull_down_inherited_attributes(classifier); false; end
  
  # Reprocess model as package should be used if you have selected any package that directly contains classes, interfaces, etc.  This usage of MagicDraw is deprecated.  Instead, please place the things you want to process in a model and select that.
  def reProcess_model_as_package; false; end
  
  # If false (the default) the director will iterate over attributes and associations in between start_class/end_class, and between start_interface/end_interface.
  # If true, attributes and associations are iterated over once all the primitives, enumerations, classes, and interfaces have already been processed.
  #          This option allows for cases when the type of the attribute or association must already exist when it is referenced.
  def defer_attributes_and_associations; false; end
  
  # Given attributes in sequence held by owning classifier, returns attributes in the sequence in which they should be traversed.
  # Can potentially filter the attributes as well.
  # Return the input unchanged if the builder should not sort the attributes.
  # This method is called from partition_properties, just before partitioning, but in principle it could be done after partitioning.
  # I think that to call to_a.sort is an assumption and really shouldn't be done.  This method should probably do nothing to the array at all and leave it up to builders to implement. I am not changing it because I don't want to break anything that already depends on this. -MF TODO: fix this someday.
  def sort_attributes(attributes); attributes.to_a.sort; end
  
  # Given a list of classifiers, returns them in the sequence they should be traversed.
  # This performs an initial sort which can optionally be overridden by a more complicated inheritance based sort done by
  #    any of sort_primitives, sort_enumerations, sort_interfaces, and/or sort_classes
  # This initial sort provides a reproducable starting point which is useful when the more complicated sort is ambiguous and stable.
  def sort_classifiers(classifiers); classifiers.sort; end
  
  # The default implementation of sort_simple and sort_complex.
  # Performs topological sort only if perform_topo_sorts is true
  def conditional_topo_sort(inheritance)
    return Topo_Sorter.limited(inheritance) if true==perform_topo_sorts
    inheritance.collect {|classifier, supers| classifier}
  end
  
  # These methods can perform a more complicated sort than sort_classifiers, based on inheritance.
  # The inheritance argument is an array of entries of the form [classifier, array-of-immediate-super-classifiers].
  # The array-of-immediate-super-classifiers contains only direct parents.
  # NOTE:  In the case of DiagramDriven_Director, array-of-immediate-super-classifiers is *NOT* filtered by appearance in diagram.
  # The inheritance array is already sorted alphabetically by classifier name.
  # These methods allow for alternative orderings, such as a topological sort (see TopoSort project) in which superclases appear first.
  def sort_simple(inheritance); conditional_topo_sort(inheritance); end
  def sort_complex(inheritance); conditional_topo_sort(inheritance);  end
  alias sort_primitives sort_simple
  alias sort_enumerations sort_simple
  alias sort_interfaces sort_complex
  alias sort_classes sort_complex
  
  # Build (outer) =============
  
  # The start_X methods all return a Boolean, true if the Director should proceed with that item.
  # The end_X methods do not need to return anything
  
  # Used when traversing a diagram
  def start_diagram(diagram); clear; true; end
  def end_diagram(diagram); end
  
  # Used when traversing a model
  def start_model(package); clear; true; end
  def end_model(package); end
  
  def start_package(package, imported_packages, classifers, roots, content_seq); package_stack << package; true; end
  def end_package(package); package_stack.pop; nil; end
  
  # The 'inbound' argument provides information about unidirectional associations that point to this class.
  # The inbound associations are useful for building documents (they tell you what is referring to this class).
  # NOTE:  In the case of DiagramDriven_Director, superClasses *IS* filtered by appearance in diagram.
  def start_class(klass, superClasses, interfaces_implemented, inbound, partition); true; end
  def end_class(klass); end
  
  def start_datatype(datatype, super_datatypes, interfaces_implemented, inbound, partition); true; end
  def end_datatype(datatype); end
  
  # The 'inbound' argument provides information about unidirectional associations that point to this interface.
  # The inbound associations are useful for building documents (they tell you what is referring to this interface).
  # NOTE:  In the case of DiagramDriven_Director, super_interfaces *IS* filtered by appearance in diagram.
  def start_interface(interface, super_interfaces, realizing_classes, inbound, partition ); true; end
  def end_interface(interface); end
  
  # The section name is one of the pkg_content_sequence return values.
  def start_section(name, package, contents); true; end
  def end_section(name); end
  
  # Start/end category of class attributes/associations
  # The category name is one of the class_cat_sequence return values.
  def start_clsAttr_cat(name, contents); true; end
  def end_clsAttr_cat(name); end
  
  # Start/end category of datatype attributes/associations
  # The category name is one of the datatype_cat_sequence return values.
  def start_datatypeAttr_cat(name, contents); true; end
  def end_datatypeAttr_cat(name); end
  
  # Start/end category of interface attributes/associations
  # The category name is one of the interface_cat_sequence return values.
  alias start_ifAttr_cat   start_clsAttr_cat
  alias end_ifAttr_cat     end_clsAttr_cat
    
  # Build (inner) =============
  
  # These methods do not need to return anything. If they do return something, it is ignored.
  # These methods are the workhorses
  
  # NOTE:  In the case of DiagramDriven_Director, super_prims *IS* filtered by appearance in diagram.
  def make_prim(prim, super_prims); end
  # You get a string from a literal via getName
  # NOTE:  In the case of DiagramDriven_Director, super_enums *IS* filtered by appearance in diagram.
  def make_enum(enum, super_enums, literals); end
  
  def process_class_attr_or_assoc(attrib, role_name, multiplicity_range, type, containing_classifier, is_last); end
  def process_class_attribute(attrib, role_name, multiplicity_range, type, containing_classifier, is_last); end
  def process_class_association(attrib, role_name, multiplicity_range, type, containing_classifier, is_last); end
  def process_class_assoc_toClass(attrib, role_name, multiplicity_range, type, containing_classifier, is_last); end
  def process_class_assoc_toIF(attrib, role_name, multiplicity_range, type, containing_classifier, is_last); end
  def process_class_association_class(attrib, role_name, multiplicity_range, type, containing_classifier, is_last); end
  def process_class_broken(attrib, role_name, multiplicity_range, type, containing_classifier, is_last)
    # classifier is undefined
    # type_name = classifier.repond_to?(:name) ? classifier.name : 'unknown'
    type_name = type.respond_to?(:name) ? type.name : 'unknown'
    container_type_name = containing_classifier.respond_to?(:name) ? containing_classifier.name : 'unknown'
    message = "Classifier #{container_type_name} has a broken attribute or association of type #{type_name} with role_name #{role_name.to_s} and multiplicity #{multiplicity_range.to_s}"
    puts message
    problems << message
  end
  def process_class_ignored(attrib, role_name, multiplicity_range, type, containing_classifier, is_last)
    # Warning is now added in #element_is_included?
    # message = "Ignored: role_name => #{role_name}, type => #{type.name}, containing_classifier => #{containing_classifier.name}"
    # Warnings << message
    # puts message
  end
  
  alias process_interface_attr_or_assoc      process_class_attr_or_assoc
  alias process_interface_attribute          process_class_attribute
  alias process_interface_association        process_class_association
  alias process_interface_assoc_toClass      process_class_assoc_toClass
  alias process_interface_assoc_toIF         process_class_assoc_toIF
  alias process_interface_association_class  process_class_association_class
  alias process_interface_broken             process_class_broken
  alias process_interface_ignored            process_class_ignored      
  
  alias process_datatype_attr_or_assoc      process_class_attr_or_assoc
  alias process_datatype_attribute          process_class_attribute
  alias process_datatype_association        process_class_association
  alias process_datatype_assoc_toClass      process_class_assoc_toClass
  alias process_datatype_assoc_toIF         process_class_assoc_toIF
  alias process_datatype_association_class  process_class_association_class
  alias process_datatype_broken             process_class_broken
  alias process_datatype_ignored            process_class_ignored
  
end

General_Builder = ModelDriven_Builder unless defined?(General_Builder) # suppress warnings # Defined for backwards compatability. ModelDriven_Builder is a better name.

# Concrete. 
# This lets you build multiple things from a single traversal (execution of the director), by distributing messages to multiple sub_builders.
# In general, the individual builders can do radically different things, provided they all respond the same way to the Query methods and the start_<X> methods.
# They need to respond the same way to the queries and start_<X> because that's the only way multiple builders can be consistently combined in a single traversal.
class Multi_Builder < ModelDriven_Builder # Inheritance is to support (probably never needed) #kind_of?  (everything is re-implemented)
  
  # An Array of subclasses of ModelDriven_Builder
  attr_reader :sub_builders
  
  def initialize(*sub_builders)
    @sub_builders = sub_builders
  end
  
  
  # A lazy initialized Array of strings
  def problems; _apply_all(:problems).flatten; end
  def result
    prob = problems
    return "Successful" if prob.empty?
    prob.join("\n")
  end
  
  def package_stack; _must_agree(:package_stack); end
  
  def clear; _apply_all(:clear); end
  
  # Queries =============
  
  # These methods can reorder the argument, or drop items from the argument
  def pkg_content_sequence(cats);  _must_agree(:pkg_content_sequence, cats); end
  def class_cat_sequence(cats);  _must_agree(:class_cat_sequence, cats); end
  def interface_cat_sequence(cats);  _must_agree(:interface_cat_sequence, cats); end
  
  # If this returns true, attributes and associations for the superclass will be provided to process_class_X and process_interface_X
  # Normally false. Useful for "flattening" hierarchies (for example, ASN.1 definitions)
  def pull_down_inherited_attributes(classifier); _must_agree(:pull_down_inherited_attributes, classifier); end
  
  # Perhaps this should use _must_agree.  Instead it passes the query on to the first sub_builder.
  def sort_attributes(attributes); sub_builders.first.sort_attributes(attributes); end
  
  # Build (outer) =============
  
  # The start_X methods all return a Boolean, true if the Director should proceed with that item.
  
  def start_model(package); clear; _must_agree(:start_model, package); end
  def end_model(package); _apply_all(:end_model, package) end
  
  def start_package(package, imported_packages, classifers, roots, content_seq)
    _must_agree(:start_package, package, imported_packages, classifers, roots, content_seq)
  end
  def end_package(package); _apply_all(:end_package, package) end
  
  # The 'inbound' argument provides information about unidirectional associations that point to this class.
  # The inbound associations are useful for building documents (they tell you what is referring to this class).
  def start_class(klass, superClasses, interfaces_implemented, inbound, partition)
    _must_agree(:start_class, klass, superClasses, interfaces_implemented, inbound, partition)
  end
  def end_class(klass); _apply_all(:end_class, klass) end
  
  # The 'inbound' argument provides information about unidirectional associations that point to this interface.
  # The inbound associations are useful for building documents (they tell you what is referring to this interface).
  def start_interface(interface, super_interfaces, realizing_classes, inbound, partition )
    _must_agree(:start_interface, interface, super_interfaces, realizing_classes, inbound, partition)
  end
  def end_interface(interface); _apply_all(:end_interface, interface) end
  
  # The section name is one of the pkg_content_sequence return values.
  def start_section(name, package, contents)
    _must_agree(:start_section, name, package, contents)
  end
  def end_section(name); _apply_all(:end_section, name) end
  
  # Start/end category of class attributes/associations
  # The category name is one of the class_cat_sequence return values.
  def start_clsAttr_cat(name, contents)
    _must_agree(:start_clsAttr_cat, name, contents)
  end
  def end_clsAttr_cat(name); _apply_all(:end_clsAttr_cat, name) end
  
  # Start/end category of interface attributes/associations
  # The category name is one of the interface_cat_sequence return values.
  def start_ifAttr_cat(name, contents)
    _must_agree(:start_ifAttr_cat, name, contents)
  end
  def end_ifAttr_cat(name); _apply_all(:end_ifAttr_cat, name) end
  
  
  # Build (inner) =============
  
  def make_prim(prim, super_prims); _apply_all(:make_prim, prim, super_prims) end
  # You get a string from a literal via getName
  def make_enum(enum, super_enums, literals); _apply_all(:make_enum, enum, super_enums, literals) end
  
  def process_class_attr_or_assoc(attrib, role_name, multiplicity_range, type, containing_classifier, is_last)
    _apply_all(:process_class_attr_or_assoc, attrib, role_name, multiplicity_range, type, containing_classifier, is_last) 
  end
  def process_class_attribute(attrib, role_name, multiplicity_range, type, containing_classifier, is_last)
    _apply_all(:process_class_attribute, attrib, role_name, multiplicity_range, type, containing_classifier, is_last)
  end
  def process_class_association(attrib, role_name, multiplicity_range, type, containing_classifier, is_last)
    _apply_all(:process_class_association, attrib, role_name, multiplicity_range, type, containing_classifier, is_last)
  end
  def process_class_assoc_toClass(attrib, role_name, multiplicity_range, type, containing_classifier, is_last)
    _apply_all(:process_class_assoc_toClass, attrib, role_name, multiplicity_range, type, containing_classifier, is_last)
  end
  def process_class_assoc_toIF(attrib, role_name, multiplicity_range, type, containing_classifier, is_last)
    _apply_all(:process_class_assoc_toIF, attrib, role_name, multiplicity_range, type, containing_classifier, is_last)
  end
  def process_class_association_class(attrib, role_name, multiplicity_range, type, containing_classifier, is_last)
    _apply_all(:process_class_association_class, attrib, role_name, multiplicity_range, type, containing_classifier, is_last)
  end
  
  
  def process_interface_attr_or_assoc(attrib, role_name, multiplicity_range, type, containing_classifier, is_last)
    _apply_all(:process_interface_attr_or_assoc, attrib, role_name, multiplicity_range, type, containing_classifier, is_last) 
  end
  def process_interface_attribute(attrib, role_name, multiplicity_range, type, containing_classifier, is_last)
    _apply_all(:process_interface_attribute, attrib, role_name, multiplicity_range, type, containing_classifier, is_last)
  end
  def process_interface_association(attrib, role_name, multiplicity_range, type, containing_classifier, is_last)
    _apply_all(:process_interface_association, attrib, role_name, multiplicity_range, type, containing_classifier, is_last)
  end
  def process_interface_assoc_toClass(attrib, role_name, multiplicity_range, type, containing_classifier, is_last)
    _apply_all(:process_interface_assoc_toClass, attrib, role_name, multiplicity_range, type, containing_classifier, is_last)
  end
  def process_interface_assoc_toIF(attrib, role_name, multiplicity_range, type, containing_classifier, is_last)
    _apply_all(:process_interface_assoc_toIF, attrib, role_name, multiplicity_range, type, containing_classifier, is_last)
  end
  def process_interface_association_class(attrib, role_name, multiplicity_range, type, containing_classifier, is_last)
    _apply_all(:process_interface_association_class, attrib, role_name, multiplicity_range, type, containing_classifier, is_last)
  end
  
  # Helpers =============
  
  # make all the sub_builders execute method_sym with the specified args.
  # Return all the results.
  def _apply_all(method_sym, *args)
    sub_builders.collect {|bldr| bldr.send(method_sym, *args) }
  end
  
  # make all the sub_builders execute method_sym with the specified args.
  # Raise an error if the sub_builders do not agree on a return value.
  # If the sub_builders do agree, then return the agreed upon value.
  def _must_agree(method_sym, *args)
    raise "Need at least one sub_builder" if sub_builders.empty?
    result_array = _apply_all(method_sym, *args)
    result_array.uniq!
    return result_array.first if 1==result_array.size
    raise "Different sub_builders failed to agree on #{method_sym.inspect}. Conflicting values: #{result_array.inspect}"
  end
  
end


class Builder::XmlMarkup
  # This is identical to #method_missing, without the closing tag. The purpose is to let you add extra content before closing.
  def _open_tag(sym, *args, &block)
    text = nil
    attrs = nil
    sym = "#{sym}:#{args.shift}" if args.first.kind_of?(::Symbol)
    args.each do |arg|
      case arg
        when ::Hash
          attrs ||= {}
          attrs.merge!(arg)
        else
          text ||= ''
          text << arg.to_s
      end
    end
    if block
       unless text.nil?
         ::Kernel::raise ::ArgumentError, "XmlMarkup cannot mix a text argument with a block"
       end
       _indent
       _start_tag(sym, attrs)
       _newline
       begin
        _nested_structures(block)
       ensure
       end
    elsif text.nil?
        _indent
        _start_tag(sym, attrs, true)
        _newline
    else
        _indent
        _start_tag(sym, attrs)
        text! text
    end
     @target
  end
  def _close_tag(sym)
    _indent
    _end_tag(sym)
    _newline
  end
end

# This wraps a Builder::XmlMarkup to provide a builder which understands data modeling semantics, but produces XML syntax.
# Builder::XmlMarkup only understands XML.
class XML_Builder < ModelDriven_Builder
  attr_reader :xml_builder
  attr_reader :xml
  
  alias b xml_builder
  
  def initialize
    buffer = String.new
    @xml = buffer
    @xml_builder = Builder::XmlMarkup.new(:target=>buffer, :indent=>2)
  end
  
  def start_model(package)
    b.instruct! :xml, :version=>"1.0", :encoding=>"UTF-8"
    true
  end
  
end

# This specialization of XML_Builder produces HTML syntax.
class HTML_Builder < XML_Builder
  
  # The HTML and BODY tags are left open
  def start_model(package)
    super(package)
    b._open_tag(:html, :xmlns=>"http://www.w3.org/1999/xhtml", :lang=>"en", "xml:lang"=>"en") {
      b.head {
        b.meta('http-equiv'=>"Content-Type", :content=>"text/html; charset=utf-8") {
        b.link(:href=>"main.css", :media=>"all", :rel=>"stylesheet", :type=>"text/css")
        b.title package.name
        } #meta
      } #head
      b._open_tag(:body) {
        b.h1 package.name
      } #body
    } #html
    true
  end
  
  def end_model(package)
    b._close_tag(:body)
    b._close_tag(:html)
  end
  
end
