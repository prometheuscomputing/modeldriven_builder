class UMLPackage
  attr_accessor :package
  attr_accessor :target_namespace
  attr_accessor :imported_namespaces
  
  # A static hash that has element id strings as keys, [prefix, uri] as value
  @@namespace_cache = Hash.new
  
  def self.namespace_cache
    @@namespace_cache
  end
  
  def namespace_cache
    @@namespace_cache
  end
  
  def uml_class_exists?(class_name)
    @package.classes.any? { |cls| cls.getName == class_name }
  end
      
  def initialize(pkg)
    @package = pkg
    @target_namespace = pkg.__Namespace
    @imported_namespaces = pkg.imported_packages.collect {|p| p.__Namespace }

  end
  
  def is_target?(uri)
    uri==target_namespace[1]
  end
  
  def is_available?(uri)
    is_target?(uri) || imported_namespaces.any? {|n| uri==n[1] } || "http://www.w3.org/2001/XMLSchema"==uri
  end
  
end
