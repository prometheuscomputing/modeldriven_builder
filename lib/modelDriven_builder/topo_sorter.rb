require 'topoSort' # This adds behaviors to AdjacencyList

class Topo_Sorter
  
  attr_accessor :graph # an AdjacencyList
  attr_accessor :vertex_lookup  # key is result of getQualifiedName.to_sym, value is a Vertex that has a domain_obj
  
  def initialize
    self.graph = AdjacencyList.new
    self.vertex_lookup = Hash.new
  end
  
  def self.complete(inheritance); new.topo_sort_complete(inheritance); end
  def self.limited(inheritance); new.topo_sort_limited(inheritance); end
  
  def classifier_to_vertex(classifier)
    name = classifier.getQualifiedName.to_sym
    vertex_lookup[name]||= Vertex.new(name, classifier)
  end
  
  # Fills the AdjacencyList with inheritance information, in the form of directed associations
  # from vertices representing a classifier (at the tail) to vertices representing direct subclasses (at the heads).
  # The inheritance argument is an array of entries, each of the form [classifier, array-of-direct-superclassifiers]
  def load_graph(inheritance)
    inheritance.each {|classifier, directsupers|
      head = classifier_to_vertex(classifier)
      directsupers.each {|directsuper| 
        tail = classifier_to_vertex(directsuper) 
        graph.create_edge(tail, head, 1)
        }
      }
  end
    
  # Returns an array of classifiers that has supers before their subclassifiers.
  # The inheritance argument is an array of entries, each of the form [classifier, array-of-direct-superclassifiers]
  # The result includes classifiers that are specified by inheritance, but are not among the first elements of the inheritance arrays.
  # See also topo_sort_limited
  def topo_sort_complete(inheritance)
    load_graph(inheritance)
    graph.topo_sort_Kahn.collect {|vertex| vertex.domain_obj }
  end
  
  # Returns an array of classifiers that has supers before their subclassifiers.
  # Identical to topo_sort_complete, but filters out classifiers that were not explicitly requested.
  def topo_sort_limited(inheritance)
    complete = topo_sort_complete(inheritance)
    requested_classifiers = inheritance.collect {|classifier, directsupers| classifer};
    complete && requested_classifiers 
  end

end