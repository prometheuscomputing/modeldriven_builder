require 'builder'
require 'singleton'
require 'modelDriven_builder/model_info'

# This modifies builder so that XML attributes are written out in alphabetical order.
# Of course the XML does not care, but the sorting makes it possible to compare
# today's output with last months (for regression testing).

module Builder
  
  class XmlMarkup < XmlBase

    # Insert the attributes (given in the hash).
    def _insert_attributes(attrs, order=[])
      return if attrs.nil?
      order = attrs.keys.sort if order.empty? # AFG added this line
      order.each do |k|
        v = attrs[k]
        @target << %{ #{k}="#{_attr_value(v)}"} if v # " WART
      end
      attrs.each do |k, v|
        @target << %{ #{k}="#{_attr_value(v)}"} unless order.member?(k) # " WART
      end
    end
  
  end

end

# THIS SHOULD NOT BE USED FOR ANY NEW CODE!!! IN THE PROCESS OF DEPRECATION 
#
# Previously called DDLGenerator, but used for many purposes other than DDL generation.
# Wraps a builder with logging and knowlege of directory.
# Note that the builder used here is *not* any subclass of ModelDriven_Builder. It's a lower-level builder
#    such as SpecBuilder (from plugin.specGen), RubyCodeBuilder (from plugin.rubyGen), 
#    RubyBuilder (from plugin.rubyGen), JavaBuilder (from plugin.rubyGen),  etc.
# Wrapping a ModelDriven_Builder like this is not a great idea, because higher level code talks to the
#    director, not the builder.
# SHOULD NOT BE A SINGLETON???
class Generator
  include Singleton
  include ModelInfoHelpers
  attr_accessor :builder
  attr_accessor :file_root
  attr_accessor :package
  
  def warnings; Warnings.list; end
  
  def self.warnings; Warnings.list; end
  def self.builder; instance.builder; end
  def self.file_root; instance.file_root; end
  
  def initialize
    Warnings.clear
    @file_root = ModelDrivenBuilderUtilies.file_root
  end
  
  def clear
    @builder = nil
    @package = nil
    @main_module_name = nil
    Warnings.clear
  end
  
  def build
    raise "Invalid Generator setup" unless builder && package
    output = builder.build(package)
    puts
    #puts output
    if warnings.empty?
      message = 'Success, no warnings!'
    else # Warnings exist
      message = "Succeeded with warnings:\n"
      message += warnings.join("\n")
    end
    puts "#{builder.class.to_s}: #{message}"
    message
  end

end


