require 'DirectedGraphSpanningTree/Edmonds'

def debug?; false; end
# Naming a method #debug in the global namespace is asking for trouble.  It already gave me some. FIXME..except who knows where this is used and what it will break when we stop doing this. -MF
def debug(&proc); proc.call if debug?; end

class Object
  def integer?
    to_s.match(/\A[+-]?\d+\Z/) == nil ? false : true
  end
  def number?
    to_s.match(/\A[+-]?\d+?(\.\d+)?\Z/) == nil ? false : true 
  end
end


module Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Element
  attr_accessor :edmonds_obj
  # Valid only if analyze_graph has executed. The value is either nil or true (would be better to include false, so that accidental omissions could be identified)
  attr_accessor :is_spanning_edge
end

module Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Property
  # This could be moved to magic_draw_nav.rb except for a few methods such as edmonds_weight_computed
  def to_s(label='PropertyImpl:', details=true)
    a = getAssociation
    answer = label.clone
    answer << "\n\tWith name: #{getName.inspect}"
    answer << "\n\tgetOwnerClass.getName: #{getOwnerClass.getName.inspect}"
    # answer << (getUMLClass ? "\n\tWith getUMLClass named #{getUMLClass.getName}" : "\n\tNo getUMLClass") # Less useful than getOwnerClass
    # answer << (getClassifier ? "\n\tHas getClassifier named: #{getClassifier.getName}" : "\n\tNo getClassifier") # In every case I've looked at so far, idential to getUMLClass
    # answer << (getOpposite ? "\n\tHas getOpposite of type #{getOpposite.getClass.getName}" : "\n\tNo getOpposite")  # Less useful than mirror_property
    # answer << (getAssociationEnd ? "\n\tHas associationEnd" : "\n\tNo associationEnd")
    # answer << getAssociationEnd.to_s("\ngetAssociationEnd", false) if getAssociationEnd
    answer << "\n\tHas mirror_property of type #{mirror_property.inspect}"
    answer << (a ? "\n\tHas getAssociation of type #{a.getClass.getName} named: #{a.getName}  _desc: #{a._desc}  _show_properties: #{a._show_properties}" : "\n\tNo getAssociation")
    # answer << (getOwningAssociation ? "\n\tHas owningAssociation named: #{getOwningAssociation.getName}" : "\n\tNo getOwningAssociation") # Have not yet seen a case where this was used
    answer << (getType ? "\n\tHas getType named: #{getType.getName} with __Qname: #{getType.__Qname}" : "\n\tNo getType")
    answer << "\n\tgetAggregation: #{getAggregation.inspect}"
    answer << "\n\trole_name: #{role_name.inspect}"
    answer << "\n\tinverse_role_name: #{inverse_role_name.inspect}"
    answer << "\n\tnavigable?: #{navigable?.inspect}"
    answer << "\n\tinverse_navigable?: #{inverse_navigable?.inspect}"
    answer << "\n\tcomposite?: #{composite?.inspect}"
    answer << "\n\tinverse_composite?: #{inverse_composite?.inspect}"
    answer << "\n\tmult: #{mult.inspect}"
    answer << "\n\tinverse_multiplicity: #{inverse_multiplicity.inspect}"
    answer << "\n\tbidirectional?: #{bidirectional?.inspect}"
    answer << "\n\tedmonds_weight_computed: #{edmonds_weight_computed.inspect}"
    answer << "\n\tisAssociationClass? : #{isAssociationClass?.inspect}"
    answer << "\n\thasQualifier: #{hasQualifier.inspect}"
    if details
      answer << "\n\tNum qualifiers: #{qualifiers.size} "
      qualifiers.each_with_index {|prop, i| answer << prop.to_s("\nQUALIFIER #{i}:", false) }
      answer << "\n\tNum inverse qualifiers: #{inverse_qualifiers.size} "
      inverse_qualifiers.each_with_index {|prop, i| answer << prop.to_s("\nINVERSE QUALIFIER #{i}:", false) }
    end
    answer << "\n"
    answer
  end
  def edmonds_weight_computed
    unidir = unidirectional?
    case
      when !isNavigable ; 0 
      when isComposite && inverse_composite? ; 0 # composition on both ends is nonsensical
      when unidir && inverse_composite? ; 0 # MagicDraw allows, but eccentric, navigation in other direction makes more sense
      when unidir && isComposite && 1==inverse_multiplicity.max ; 40 # containment semantics is pretty close to compositon semantics.
      when unidir && isComposite && 1<inverse_multiplicity.max ; 0 # MagicDraw allows, but containment by multiple instances is nonsensical
      when unidir && !isComposite ; 20 # Try to honor user specified direction
      when !unidir && !isComposite && !inverse_composite? ; 5 # Two directions treated equally, let Edmonds algorithm decide which is preferable
      when !unidir && isComposite ; 10 
      when !unidir && inverse_composite? ; 0  # Prefer tail composite
      else 1
    end
  end
  def edmonds_weight
    s = getAssociation ? getAssociation.getName.to_s : ''
    debug {
      puts "================="
      puts to_s
      op = mirror_property
      puts  op ? op.to_s('REVERSE:') : 'NO REVERSE'
      puts "================="
      }
    return s.to_i if s.integer?
    edmonds_weight_computed
  end
end



# This can be mixed in to identify edges which are part of the "maximum flow" tree.
module Edmonds_Filter
  
  # Keys are IDs of Properties not pruned by Edmond's algorithm, each value is an Array of Edmonds edges
  # There is only a single entry in these arrays, unless the association is a class association (in which case there may two entries, caused by the split)
  attr_accessor :kept_hash 
  
  def kept?(property); kept_hash.key?(property.getID); end
  
  # Return true if the Edmond's algorithm did not prune the half of the association that starts from an association class.
  def kept_ac?(property)
    edges = kept_hash[property.getID]
    return false unless edges
    ed_vert = property.getAssociation.edmonds_obj
    edges.detect {|edge| edge.from==ed_vert}
  end
  
  def uml_vertices(*packages)
    packages.collect {|pkg| pkg.interfaces + pkg.classes + pkg.association_classes }.flatten
  end
  
  def nonInterface_vertices(*packages)
    packages.collect {|pkg| pkg.classes + pkg.association_classes }.flatten
  end
  
  # Determine which associations should be represented in XML by child elements.
  # This does NOT recurse into packages.
  def analyze_graph(*packages)
    graph = AdjacencyList.new
    # Create an Edmonds vertex for each UML vertex. Could have eliminated
    # this by allowing any Object to be an Edmonds vertex.
    uml_vertices(*packages).each {|v| v.edmonds_obj= Vertex.new(v.getName.to_sym, v) }
    # The rest of the schema generator does not currently support associations that start from interfaces, so we ignore them here also.
    # This section is similar to _build_associations_for_cls in version of plugin.schemaGen that does not use ModelDriven_Director
    sorted_cls = nonInterface_vertices(*packages).sort! {|a, b| a.getName <=> b.getName }
    sorted_cls.each {|cls|
      next unless cls.interface? || cls.class?
      debug { 
        puts "PROCESSING associations of vertex #{cls.getName}" 
        puts "ModelHelper.getAssociationEnds(cls): #{ModelHelper.getAssociationEnds(cls).to_a.collect {|prop| prop.getOwnerClass.getName }.inspect}"
        puts "ModelHelper.attributes(cls): #{ModelHelper.attributes(cls).to_a.collect {|prop| prop.getOwnerClass.getName }.inspect}"
        puts "cls.getOwnedAttribute: #{cls.getOwnedAttribute.collect {|prop| prop.mirror_property}.compact.collect {|prop| prop.getOwnerClass.getName}.inspect}"
        }
      src = cls.edmonds_obj # Edmonds vertex for source of the association
      (debug {puts "Ignored all associations from #{cls.getName} due to unexpectedly missing edmonds_obj"}; next) unless src
      cls.getOwnedAttribute.to_array.sort.each {|attribute|
        type = attribute.getType
        next unless attribute.navigable? && type && (type.interface? || type.class?)
        dest = type.edmonds_obj # Edmonds vertex at other end of the association
        (debug {puts "Ignored association from #{cls.getName} to #{type.getName} due to missing edmonds_obj at head end" }; next) unless dest 
        if  attribute.isAssociationClass?
            # Split the association, and insert the association class in between (for purposes of Edmonds algorithm)
            assoc = attribute.getAssociation
            ac = assoc.edmonds_obj # Edmonds vertex for association class, rather than the other end of association
            (Warnings << "Edmonds will ignore associationClass #{assoc.getName} due to missing vertex object"; next) unless ac
            weight = attribute.edmonds_weight
            graph.create_edge(src, ac, weight, nil, attribute)
            graph.create_edge(ac, dest, weight, nil, attribute)
        else
          graph.create_edge(src, dest, attribute.edmonds_weight, nil, attribute)
        end
        }
      }
    debug {
      puts "Edges =============" 
      graph.edges.sort.each {|edge| puts "#{edge.to_s} weight: #{edge.weight}" }
      puts "Edges =============" 
      }
    edmonds = Edmonds.new(graph)
    self.kept_hash=Hash.new
    kept_edges = edmonds.spanning_edges
    kept_edges.each {|edge| 
      debug {puts "KEEP #{edge.to_s} from #{edge.from.domain_obj.getName}" }
      assoc = edge.domain_obj
      assoc.is_spanning_edge = true
      (kept_hash[assoc.getID]||=Array.new) << edge
      }
    debug {puts "Number kept: #{kept_hash.size}"}
  end
  

end
