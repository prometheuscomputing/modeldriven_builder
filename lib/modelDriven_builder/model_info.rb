module ModelInfoHelpers

  def clear_model_options
    puts "Clearing model_options"
    @model_name          = nil
    @additional_plugins  = nil
    @application_options = nil
    @project_version     = nil
  end
  
  # doesn't work for getting the entire array for a particular tag but we don't typically need to do that anyway, at least not at this point
  def model_options(option = nil)
    root_package = get_root_package
    # Attempt to get main module name from options
    options = root_package.get_tag_values_hash('options', :fail_silently)
    return options if option.nil?
    return options[option.to_s].first if options[option.to_s]
    return nil
  end
  
  def model_name
    unless @model_name
      project_name_from_options = model_options 'project_name'
      if project_name_from_options
        @model_name = format_model_name(project_name_from_options)
      else
        # We are asking for confirmation of the project name if there was no project name in options
        project_name = format_model_name(get_root_package.getName)
        @model_name = Java::javax.swing.JOptionPane.showInputDialog("Package name to generate into", project_name)
      end
    end
    # puts "Project Name is #{@model_name}"
    @model_name
  end
  
  def additional_plugins
    @additional_plugins ||= _split_vals(model_options('plugins'))
  end
  
  def application_options
    unless @application_options
      @application_options = {}
      (_split_vals(model_options('application_options'))).each{|opt| @application_options[opt] = true}
    end
    @application_options
  end
  
  def _split_vals str
    return [] if str.nil?
    str.split(/[\s,;]+/).collect{|x| x.to_sym}
  end
    
  
  def attributes_are_compositions?
    model_options('attributes_are_composition') || false
  end
  
  def title
    name_parts(model_name).join(" ")
  end
  
  def model_name_generated
    model_name + "Generated"
  end
  alias_method :main_module_name, :model_name_generated #backwards compatibility...maybe
  alias_method :generated_model_name, :model_name_generated # because I get it backwards

  def generated_gem_name
    to_gem_name model_name_generated
  end
  alias_method :gem_name_generated, :generated_gem_name # because I get it backwards

  def gem_name
    to_gem_name model_name
  end

  def model_version
    unless @project_version
      root_package = get_root_package
      # First try to get version from options
      @project_version = root_package.get_tag_value(:options, :version)
      unless @project_version && @project_version.is_a?(String) && !@project_version.empty?
        project_name = Java::com.nomagic.magicdraw.core.Project.getProject(root_package).getName
        @project_version = project_name.slice(/\d+\.\d+\.\d+.*/)
      end
      unless @project_version && @project_version.is_a?(String) && !@project_version.empty?
        @project_version = '0.0.0'
      end
    end
    puts "Project Version is #{@project_version}"
    @project_version
  end
  alias_method :project_version, :model_version


  def get_root_package
    selections = $SELECTIONS
    raise "$SELECTIONS is nil" if selections.nil?
    raise "$SELECTIONS is empty" if selections.empty?
    if selections.count == 1
      selections.first
    else
      # it isn't really possible to know that you are choosing the right package as root if there isn't an actual root package
      top_level_packages = selections.collect{|pkg| get_package_owner_or_self pkg}.uniq.compact
      raise "There was an unexpected problem determining which package is the root package" if top_level_packages.empty?
      arbitrarily_returned_guess_at_root_package = top_level_packages.first
    end
  end
  
  def get_package_owner_or_self pkg
     owner = pkg.getOwner
     if owner
       get_package_owner owner
     else
       pkg
     end
  end
  
  private
  def format_model_name str
    ret = str.split(/ |_|\s+|(?=[A-Z][a-z])|(?<=[a-z])(?=[A-Z]$)/).collect do |p|
      chars = p.split ""
      chars[0].upcase!
      p = chars.join
      p = p + "_" if chars[-1] =~ /[A-Z]/
      p
    end.join
    ret.gsub!(/_$/,"")
    ret
  end
  
  def to_gem_name str
    name_parts(str).collect{|w| w.downcase}.join("_")
  end
  
end

load 'modelDriven_builder/helper.rb'
class ModelInfo
  include Singleton
  include ModelInfoHelpers
  include RubyBuilderHelper
end
