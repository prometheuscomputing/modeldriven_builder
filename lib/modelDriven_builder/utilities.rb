module ModelDrivenBuilderUtilities
  def self.file_root
    file_root = File.join(Prometheus, 'Generated_Code')
    mkdir_p(file_root) unless File.exists?(file_root)
    raise "#{file_root} already exists, but is not a directory" unless File.directory?(file_root)
    file_root
  end
  
  def self.save_to_file(filename, output, subdirs = [], executable = false)
    Dir.chdir(ModelDrivenBuilderUtilities.file_root)
    subdirs.each do |dir|
      Dir.mkdir(dir) unless File.exists?(dir)
      raise "#{dir} already exists, but is not a directory" unless File.directory?(dir)
      Dir.chdir(dir)
    end
    f = File.open(filename, 'w+') {|file| file << output}
    File.chmod(0754, File.expand_path(filename)) if executable # TODO: this is unix-specific, should check os maybe?
    f
  end
end