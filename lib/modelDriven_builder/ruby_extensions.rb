class String
  def conform_to_naming_convention(options = {})
    self.gsub("-", "_")
  end
end

class Symbol
  def <=>(arg); to_s<=>arg.to_s; end
end

class String
  alias broken_compare <=>
  def <=>(arg); broken_compare(arg.to_s); end
end
