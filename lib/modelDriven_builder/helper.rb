# moved here from rubyBuilderHelper/helper.rb

require 'sequel'
require 'sequel/extensions/inflector'
require 'modelDriven_builder/ruby_extensions'

def_or_redef :SHORT_NAME, true
def_or_redef :PLURALIZATION_SETTINGS, {
  :types => true,
  :roles => false
}

module RubyBuilderHelper
	def_or_redef :RESERVED_WORDS, ['super', 'class', 'def', 'when', 'else', 'end', 'if', /^case$/i]
	def_or_redef :INVERSE_PREFIX, ''  # Should be something like 'inverse_' to use getters like 'step_inverse_actors'

  # Logic deriving the role name. Used to prevent the case that there
  # is not an explicit role on this side.
  def get_role role, inverse_role, association
    association_name = if association.kind_of?(String)
                         association
                       elsif association.respond_to?(:getName)
                         association.getName
                       else
                         raise ArgumentError, "unrecognized argument " +
                           "#{association}"
                       end

    if role && !role.empty?
      role_name = role
    elsif association_name && !association_name.empty? && (inverse_role && !inverse_role.empty?)
      role_name = association_name
    elsif inverse_role && !inverse_role.empty? && (!defined?(SHORT_NAME) || !SHORT_NAME)
      role_name = INVERSE_PREFIX + inverse_role
    else
      role_name = ''
    end
    
    role_name
  end

  # Get the getter_name for an association, given the property for the association
  # Optionally accepts the property for the opposite end of the association to 
  # speed processing (it is derived if not specified)
  def getter_name(property, other_property = nil, options = {}) # do_pluralize = true, do_downcase = true, do_clean = false
    default_options = {:pluralize => true, :downcase => true, :clean => false}
    
    # OK, we need to preserve options as the same object here because it will need to be modified and those modifications will need to be available in the scope that called this method.  Here is how we'll do it.  Kinda funky but it should work.
    options.merge!(default_options.merge!(options))
    association = property.getAssociation
    if association
      # Get the other_side of the association (if not passed)
      other_property ||= _get_opposite_association_end(property)
    
      # Set up vars
      type = property.getType.getName # Type name of the opposite end of the association

      # boolean for pluralizing or not
      options[:pluralize] = options[:pluralize] && (property.is_multiple? || association.isAssociationClass?)

      role_name = get_role(property.getName, other_property.getName, association)
      # If true, this means there really isn't a role name on this end of this
      # class association, even though get_role returns one.  get_role gets the
      # role_name from the association name if the association name is defined.
      # The association name of a class association object is usually the name
      # of the association class itself
      if options[:pluralize] && association.isAssociationClass? && 
         association.getName.downcase == role_name.downcase
        type = association.getName
        role_name = ''
      end

      is_role_name_empty = role_name.empty?
      if defined?(SHORT_NAME) && SHORT_NAME
        name = is_role_name_empty ? getter_name_from_type(type, options) : getter_name_from_role(role_name, options)
      else
        name = type
        unless is_role_name_empty
          name += '_' + getter_name_from_role(role_name, options)
        end
      end
      getter = options[:clean] ? clean(name) : name
    else # No association exists
      #NOTE: currently, pluralize and clean options do not apply here
      
      # If an association does not exist, return the name of the property
      getter = conform(property.getName, options)
    end
    options[:downcase] ? getter.downcase : getter
  end
  
  # Parses the getter name for underscores or camel casing, returning a human readable title
  def title_name attribute, other_attribute = nil
    options = {:pluralize => false, :downcase => false, :clean => false}
    # NOTE: to_title is defined in magicdraw_extensions/ruby_extensions_fromHelper -SD
    ret = getter_name(attribute, other_attribute, options).to_title
    # Have to pluralize after getting the name otherwise formatting can go wonky.  Only want to pluralize if title is derived from type, not role
    (attribute.is_multiple? && options[:derived_from_type]) ? ret.pluralize : ret
  end

  def getter_name_from_type type, options = {}
    options[:derived_from_type] = true
    if PLURALIZATION_SETTINGS[:types] && options[:pluralize]
      conform(type, options).pluralize
    else
      conform(type, options)
    end
  end

  def getter_name_from_role role, options = {}
    options[:derived_from_role] = true
    if PLURALIZATION_SETTINGS[:roles] && options[:pluralize]
      conform(role, options).pluralize
    else
      conform(role, options)
    end
  end

  # Given a property, gets the getter name for the opposite side of the association if there is an association, otherwise just gets the name from the owner's type (i.e. the property was an attribute)
  def inverse_getter_name(property, options = {})
    if association = property.getAssociation
      test_side = ModelHelper.getFirstMemberEnd(association)
      orig_side = (test_side.equal?(property)) ? ModelHelper.getSecondMemberEnd(association) : test_side
      ret = getter_name(orig_side, property, options)
    else
      options = {:pluralize => false, :downcase => true}.merge options
      ret = getter_name_from_type(property.getOwner.getName, options)
    end
    puts "UHOH, no inverse getter for #{property.getName}" unless ret
    ret
  end

  def clean string
    is_reserved_word = false
    if string
      RESERVED_WORDS.each do |reserved_word|
        case reserved_word
        when String # Perform case sensitive match
          is_reserved_word = true if string == reserved_word
        when Regexp # Perform match according to regexp
          is_reserved_word = true if string.match(reserved_word)
        else
          raise "Reserved words should only contain Strings and Regexps"
        end

        if is_reserved_word
          warning = "Ruby reserved word #{string} has been mapped to _#{string}"
          warnings << warning unless warnings.include? warning

          # Prepend an underscore to prevent conflict with reserved word
          return '_' + string
        end
      end
    end
    string
  end
  
  # This should eventually be moved into the naming service TODO
  def name_parts str
    splitter = Regexp.new "_+| +|(?<=[A-Za-z])(?=[A-Z][a-z])" # splitter
    regex0 = Regexp.new "^[A-Z0-9]+$" # matches all caps and numbers
    regex1 = Regexp.new "^([A-Za-z]*[a-z]+)([A-Z][A-Z0-9]+)$" # two part match and rebuild
    regex2 = Regexp.new "^([A-Za-z]+[a-z0-9]+)([A-Z][a-z0-9]+)$" # two part match and rebuild
    regex3 = Regexp.new "^([A-Za-z]+[a-z0-9]+)([A-Z]+)$" # two part match and rebuild
    regex4 = Regexp.new "^([A-Z][A-Z0-9]+)([a-z]+)$" # two part match and rebuild
    regex5 = Regexp.new "^([^0-9a-z]+[^0-9A-Z])([0-9]+)([^0-9]+)$" # three part match and rebuild
    match_anything = Regexp.new "^(.*)(.*)$"
  
    splits = str.split splitter
    matches = splits.compact.flatten.collect do |part|
      if part =~ regex0
        part
      else
        matched = false
        [regex1, regex2, regex3, regex4, regex5, match_anything].each do |regex|
          next if matched
          next unless part =~ regex
          matches = (regex.match part).captures
          if matches.inject(true) {|answer, match| answer && !match.empty?}
            matched = (matches[0..-2].join + "_" + matches[-1])
          else
            matched = part
          end
        end
        matched
      end
    end
  end
  
  # This should eventually be moved into the naming service TODO...and also the downcasing bit might need to be separated out
  def conform(str, options = {})
    ret = name_parts(str.conform_to_naming_convention).join("_")
    ret.downcase! if options[:downcase]
    ret
  end

	def warnings
		raise NoMethodError, "Concrete class #{self.class.name} must implement " +
			"#warnings with an Array as a return value"
	end
end
