require "modelDriven_builder/edmonds_filter"

class Abstract_Director
  MD_ASSOC_CLASS = Java::ComNomagicUml2ExtMagicdrawClassesMdassociationclassesImpl::AssociationClassImpl  unless defined?(MD_ASSOC_CLASS) # suppress warnings
  MD_ASSOC = Java::ComNomagicUml2ExtMagicdrawClassesMdkernelImpl::AssociationImpl unless defined?(MD_ASSOC) # suppress warnings
  MD_PROPERTY = Java::ComNomagicUml2ExtMagicdrawClassesMdkernelImpl::PropertyImpl unless defined?(MD_PROPERTY) # suppress warnings
  
  attr_accessor :builder
  
  # A Boolean, defaults to false. If true, Edmonds_Filter is mixed in (at the instance level) and associations will have is_spanning_edge set before the traversal begins.
  attr_accessor :finds_spanning_tree
  
  # Specifies what kinds of things you want to traverse inside a package. 
  # These are messages sent to a Package to get content to traverse. pkg_content_seq is required to be a subset (possibly in a different order).
  # The corresponding getter automatically removes :packages if flatten_packages is true.
  # Defaults to DEFAULT_PACKAGE_CONTENT
  attr_writer :pkg_content
  
  # A Boolean (defaults to true):
  #  If true, all packages (including sub-packages) are handled at the same level, start_package is NOT sent when you are already inside a package
  #  If false, sub-packages are handled within parent packages
  attr_accessor :flatten_packages
  
  # If flatten_packages is true, this will be synonymous with traversal_roots
  attr_accessor :packages_being_processed
  
  # An array of Strings, defaults to DEFAULT_IGNORED_PACKAGE_NAMES
  attr_accessor :ignored_package_names
  
  DEFAULT_IGNORED_PACKAGE_NAMES = ['UML2 Metamodel', 'Primitives (Class based)'] unless defined?(DEFAULT_IGNORED_PACKAGE_NAMES) # suppress warnings
  DEFAULT_PACKAGE_CONTENT = [:packages, :primitives, :enumerations, :classes, :interfaces] unless defined?(DEFAULT_PACKAGE_CONTENT) # suppress warnings
  
  def initialize(builder, finds_spanning_tree=false, flatten_packages=true)
    ModelInfo.instance.clear_model_options
    self.flatten_packages = flatten_packages
    self.pkg_content = DEFAULT_PACKAGE_CONTENT
    self.ignored_package_names = DEFAULT_IGNORED_PACKAGE_NAMES
    self.builder = builder
    extend(Edmonds_Filter) if finds_spanning_tree
    self.finds_spanning_tree = finds_spanning_tree
  end
  
  def packages_being_processed
    raise "#packages_being_processed must be overriden by subclass of AbstractDirector"
  end
  
  def packages_being_processed= packages
    raise "#packages_being_processed= must be overriden by subclass of AbstractDirector"
  end
  # Helper methods ====================
  
  def properties_for_classifier(classifier); classifier.getOwnedAttribute; end
  
  def pkg_content(recursion_allowed=true)
    return @pkg_content if recursion_allowed && !flatten_packages
    @pkg_content - [:packages]
  end
  
  # Categories of properties.  An property may be placed in more than one category.
  # NOTE: the categories for a given property are determined by #partition_properties, based on:
  #       * the type of data, and the following methods defined in magic_draw_nav.rb: primitive?,  enumeration?,  interface?, and class?
  #       * as well as  isAssociationClass? defined on the property itself.
  def property_categories; [:attr_or_assoc, :attribute, :association, :assoc_toClass, :assoc_toIF, :association_class, :broken, :ignored]; end
  
  # Returns a Hash of properties that have been sorted according to the keys in property_categories.
  def partition_properties(classifier, super_classifiers, answer=Hash.new)
    property_categories.each {|type| answer[type]=Array.new } if answer.empty?
    super_classifiers.each {|aSuper|partition_properties(aSuper, aSuper.super_classifiers, answer) } if builder.pull_down_inherited_attributes(classifier)
    unless classifier.respond_to?(:getDirectProperties)
      puts classifier.methods.sort.inspect
      puts classifier.inspect
      puts classifier.getQualifiedName
    end
    properties = classifier.getDirectProperties.to_a
    
    # begin
    #   properties = classifier.getDirectProperties.to_a
    # rescue Exception => e
    #   puts "*"*40
    #   puts e.backtrace
    #   puts "*"*40
    #   puts e.message
    #   puts classifier.getQualifiedName
    #   raise e
    # end
    properties = builder.sort_attributes(properties)
    properties.each do |property|
      
      type = property.getType
      next answer[:broken]<<property unless type
      if ignore_property?(property)
        # puts "Classifier #{classifier.name} had an ignored property:"
        next answer[:ignored]<<property
      end
      cats = case
        when type.primitive?   ; [:attr_or_assoc, :attribute]
        when type.enumeration? ; [:attr_or_assoc, :attribute]
        when property.isAssociationClass? ; [:association_class]
        when type.interface?   ; [:attr_or_assoc, :association, :assoc_toIF]
        when type.class?       ; [:attr_or_assoc, :association, :assoc_toClass]
        when type.datatype?    ; [:attr_or_assoc, :association, :assoc_toClass] # I don't really know what assoc_toClass is because I don't use it.  Should a datatype be in there?  They end up as Ruby classes so I'm leaving it for now.  MF - 4/20/2015
        else [:broken]
      end
      puts "This property is BROKEN! -- #{property.name}: #{type.getQualifiedName} -- #{property.inspect}" if cats.include?(:broken)
      cats.each {|cat| answer[cat]<< property }
    end
    
    answer
  end
  
  def ignore_property?(property)
    !element_is_included?(property)
  end
  
  # At this point I think this works only for packages, classes, association classes, attributes, and associations - MF
  # the continue param is needed to stop infinite recursion when checking ownership of the classes joined by an association class
    # TODO This depends on functionality defined in ModelDriven_Director.  @packages_being_processed being populated and right now that only happens in ModelDriven_Director.
  def element_is_included?(model_element, continue = true)
    name = model_element.getQualifiedName 

    # Hackish way of allowing inclusion if we are passed the package that includes PrimitiveTypes (since I don't know how to get ahold of the package object and stick it into packages_being_processed)
    # TODO -- check into #primitive? so we can handle subclasses of Primitive...or maybe not because these ought to be in a different package than the MD built-ins.
    extra_allowed_package_names =  ['UML Standard Profile::UML2 Metamodel::PrimitiveTypes', 'UML Standard Profile::MagicDraw Profile::datatypes']
    
    # check to see if we were passed an included package (rather than an element in a package)
    if packages_being_processed.include?(model_element) || extra_allowed_package_names.include?(model_element.getQualifiedName.strip)
      op = model_element
      ret = true
    else
      if model_element.is_a? MD_PROPERTY
        ret = element_is_included?(model_element.getType, continue)
      elsif model_element.is_a? MD_ASSOC_CLASS
        ret = element_is_included?(model_element.getOwningPackage)
        end_one = ModelHelper.getFirstMemberEnd(model_element)
        end_two = ModelHelper.getSecondMemberEnd(model_element)
        ret = ret && element_is_included?(end_one, false) && element_is_included?(end_two, false)
      else
        op = model_element.getOwningPackage if model_element.respond_to?(:getOwningPackage)
        op = model_element.getOwner.getOwningPackage if op.nil? && model_element.respond_to?(:getOwner)
        if op.nil?
          raise "WARNING.  Can not determine package ownership for #{model_element.name} -- #{model_element.inspect}"
          ret = false # in case we decide not to raise...
        end
        if extra_allowed_package_names.include? op.getQualifiedName
          ret = true
        else
          ret = packages_being_processed.include? op # This fails when an association is owned by Data (and it should -- the owner should be changed to an included package)
          # puts "ret false for #{model_element.name} with op:#{op.name}" if !!ret == false
        end
      end
    end
    association_ok = true
    if continue && model_element.respond_to?(:getAssociation)
      association = model_element.getAssociation
      if association.is_a? MD_ASSOC_CLASS
        association_ok = element_is_included?(association)
      elsif association.is_a? MD_ASSOC
        association_ok = element_is_included?(association, false)
      elsif model_element.is_a? MD_PROPERTY
        # no need to do anything
      else
        # What else could it be?
      end
    end
    ret = ret && association_ok
    unless ret
      name = 'Unnamed Element' if name.nil? || name.empty?
      message = "Not including the #{model_element.class.to_s.demodulize}, with qualified name:#{name} -- association ok: #{!!association_ok}"
      # puts message
      Warnings << message
    end
    # puts "#{ret ? 'Included' : 'Excluded'} #{model_element.class.to_s.demodulize} #{name}" unless name.nil? || name.empty?
    ret
  end
  
  def elements_are_included?(*model_elements)
    input_packages = model_elements.collect do |me|
      element_is_included? me
    end
    input_packages.reduce{|r,e| r && e}
  end
  
  def inheritance_generalization(classifiers)
    classifiers.collect {|classifier| [classifier, classifier.getGeneral] }
  end
  
  def inheritance_super(classifiers)
    classifiers.collect {|klass| [klass, klass.getSuperClass] }
  end
  
end



# Traverses a model. Along the way, it sends messages to an instance of a subclass of ModelDriven_Builder.
# This director differs slightly from the gang-of-four builder pattern, in that this director adjusts it's traversal based on
# information provided by the builder:
#  * The builder can tell the director the sequence of some aspects of the traveral, or even ignore some categories, according to the results of the builder's query methods.
#  * The builder can tell the director to ignore a specific class, interface, etc (according to the result of the start_<X> methods)
class ModelDriven_Director < Abstract_Director

  def packages_being_processed
    @packages_being_processed
  end
  
  def packages_being_processed= packages
    @packages_being_processed = packages
  end
  
  # This includes imported packages as well as directly contained packages.
  # Could be a method on Package in MagicDraw extensions, except for the name filter.
  def effective_child_packages_of(package)
    imps = package.getPackageImport.collect { |import| import.getImportedPackage }
    pkgs = imps + package.packages
    pkgs.uniq! # Not certain this is effective
    child_packages = pkgs.reject {|pkg| ignored_package_names.member?(pkg.getName) }
    # child_packages.each{|cp| puts "Effective child package: #{cp.name}"}
    child_packages
  end
  
  # Returns answer (which is an Array).
  # The answer contains all child packages, to arbitrary depth.
  # The passed in packages are included.
  # The answer is ordered according to depth-first traversal.
  def collect_packages(answer, *packages)
    packages.each do |pkg|
      next if answer.member?(pkg) || ignored_package_names.member?(pkg.getName)
      answer << pkg
      kids = effective_child_packages_of(pkg)
      # puts "#{pkg.name} child packages: #{kids.collect{|k| k.name}.join(', ')}"
      collect_packages(answer, *kids) if kids.any?
    end
    answer.uniq!
    # answer.each{|p| puts "Collected package: #{p.name}"}
    answer
  end
  
  # Used if we really are processing a model or if we are processing a package that contains only packages and does not directly contain any classes, interfaces, etc.
  # The model is treated as a container of packages to be traversed.
  # We presume that the model does not directly contain classes or interfaces.
  def _process_model(model)
    return unless builder.start_model(model)
    traversal_roots = effective_child_packages_of(model) # model is not included!
    process_model_helper(traversal_roots)
    builder.end_model(model)
  end
  
  # Only accepts an array!
  def process_multiple_traversal_roots(traversal_roots)
    return unless builder.start_model(traversal_roots) # builder has to be able to handle an array here or you get nothing
    process_model_helper(traversal_roots)
    builder.end_model(traversal_roots) # builder has to be able to handle an array here
  end
  
  # Parameter should be a single package
  def process_single_traversal_root(traversal_root)
    return unless builder.start_model(traversal_root) # builder has to be able to handle a single package here
    traversal_roots = [traversal_root]
    process_model_helper(traversal_roots)
    builder.end_model(traversal_root) # builder has to be able to handle a single package here
  end
  
  # Common / core behavior of model processing
  def process_model_helper(traversal_roots)
    self.packages_being_processed = collect_packages(Array.new, *traversal_roots) # Does include traversal_roots
    puts "Packages being processed: #{packages_being_processed.collect{|p| p.name}.join(', ')}"
    traversal_roots = packages_being_processed if flatten_packages
    analyze_graph(*packages_being_processed) if finds_spanning_tree
    traversal_roots.each {|pkg| process_package(pkg, true)}
  end
  
  def process_packages(package)
    effective_child_packages_of(package).each {|pkg| process_package(pkg, true) }
  end
  
  # in order to preserve some backwards compatibility this can accept either a model or a package or an array of packages
  def process_model(model_or_packages)
    model_or_packages = Array(model_or_packages)
    # puts "Size is #{model_or_packages.size}"
    # puts "reProcess_model_as_package is #{builder.reProcess_model_as_package}"
    if model_or_packages.size == 1 && !builder.reProcess_model_as_package
      _process_model(model_or_packages.first)
    else
      puts "\n\n=====================\nWARNING - reProcess_model_as_package is deprecated. Please, put your package or packages into a model and select that rather than selecting a package or packages directly...and then unset the reProcess_model_as_package flag.\n=====================\n\n"
      if model_or_packages.size == 1
        process_single_traversal_root(model_or_packages.first)
      else
        process_multiple_traversal_roots(model_or_packages)
      end
    end
  end
  
  # Child packages are processed only if recursion_allowed is true AND flatten_packages is false AND :packages is a member of pkg_content.
  def process_package(package, recursion_allowed)
    # self.processed_packages << package
    # puts "processing_package #{package.name}"
    content_seq = builder.pkg_content_sequence(pkg_content(recursion_allowed))
    classifiers = content_seq.collect {|part| package.send(part)}.flatten
    return unless builder.start_package(package, package.imported_packages, classifiers, package.roots.sort, content_seq)
    content_seq.each {|part|
      # puts "Doing process_#{part} for #{package.name}"
      send "process_#{part}", package
    }
    builder.end_package(package)
  end
  
  # Methods dispatched by #process ============
  
  def process_primitives(package)
    contents = builder.sort_classifiers(package.primitives - package.enumerations)
    return unless builder.start_section(:primitives, package, contents)
    contents = builder.sort_primitives inheritance_generalization(contents)
    contents.each {|prim| builder.make_prim(prim, prim.getGeneral) }
    builder.end_section(:primitives)
  end
  
  def process_enumerations(package)
    contents = builder.sort_classifiers(package.enumerations)
    return unless builder.start_section(:enumerations, package, contents)
    contents = builder.sort_enumerations inheritance_generalization(contents)
    contents.each {|enum| builder.make_enum(enum, enum.getGeneral, enum.literals) }
    builder.end_section(:enumerations)
  end
  
  def process_datatypes(package)
    dt = package.datatypes
    contents = builder.sort_classifiers(dt)  # why are we doing this and then replacing it two lines later?
    return unless builder.start_section(:datatypes, package, contents)
    contents = builder.sort_classes inheritance_generalization(contents)
    contents = contents.select{|datatype| element_is_included? datatype} # gets rid of association classes that associate non-included classifiers
    contents.each {|datatype| process_datatype(datatype, package)}
    builder.end_section(:datatypes)
  end
  
  def process_datatype(datatype, package)
    super_datatypes = datatype.getGeneral
    if super_datatypes.any?
      raise "Can not process model because #{datatype.name} has a super-datatype that is not part of the selected model" unless elements_are_included?(*super_datatypes) # raise rather than ignore
    end
    partition = partition_properties(datatype, super_datatypes)
    continue = builder.start_datatype(datatype, super_datatypes, datatype.__interfaces_implemented, datatype.inbound_properties(package), partition )
    # puts "Not continuing with class #{datatype.name}" unless continue
    return unless continue
    builder.datatype_cat_sequence(property_categories).each do |category|
      properties = partition[category]
      # puts "    processing #{klass.name} #{category}s:" if properties.any?
      # properties.each{|p| puts "        #{p.role_name}"}
      return unless builder.start_datatypeAttr_cat(category, properties)
      max_i = properties.size-1
      properties.each_with_index do |prop, i| 
        is_last = i==max_i
        # puts "process_class_#{category} for #{klass.name}.#{prop.role_name}" if builder.class.to_s =~ /fragment/i
        builder.send("process_datatype_#{category}", prop, prop.role_name, prop.mult, prop.type, datatype, is_last)
      end
      builder.end_datatypeAttr_cat(category)
    end
    builder.end_datatype(datatype)
  end
  
  def process_classes(package)
    contents = builder.sort_classifiers(package.classes)  # why are we doing this and then replacing it two lines later?
    return unless builder.start_section(:classes, package, contents)
    contents = builder.sort_classes inheritance_super(contents)
    contents = contents.select{|klass| element_is_included? klass} # gets rid of association classes that associate non-included classifiers
    contents.each {|klass| process_class(klass, package)}
    builder.end_section(:classes)
  end
  
  def process_class(klass, package)
    super_classes = klass.getSuperClass
    if super_classes.any?
      # FIXME this is kind of a problem if you actually do want to generate and ignore the superclass...which, yes, can actually happen
      raise "Can not process model because #{klass.name} has a superclass that is not part of the selected model" unless elements_are_included?(*super_classes) # raise rather than ignore
    end
    partition = partition_properties(klass, super_classes)
    continue = builder.start_class(klass, super_classes, klass.__interfaces_implemented, klass.inbound_properties(package), partition )
    # puts "Not continuing with class #{klass.name}" unless continue
    return unless continue
    builder.class_cat_sequence(property_categories).each do |category|
      properties = partition[category]
      # puts "    processing #{klass.name} #{category}s:" if properties.any?
      # properties.each{|p| puts "        #{p.role_name}"}
      return unless builder.start_clsAttr_cat(category, properties)
      max_i = properties.size-1
      properties.each_with_index do |prop, i| 
        is_last = i==max_i
        # puts "process_class_#{category} for #{klass.name}.#{prop.role_name}" if builder.class.to_s =~ /fragment/i
        builder.send("process_class_#{category}", prop, prop.role_name, prop.mult, prop.type, klass, is_last)
      end
      builder.end_clsAttr_cat(category)
    end
    builder.end_class(klass)
  end
  
  def process_interfaces(package)
    contents = builder.sort_classifiers(package.interfaces)
    return unless builder.start_section(:interfaces, package, contents)
    contents = builder.sort_interfaces inheritance_generalization(contents)
    contents.each {|interface| process_interface(interface, package)}
    builder.end_section(:interfaces)
  end
  
  def process_interface(interface, package)
    realizations = interface.get_interfaceRealizationOfContract
    realizers = realizations.collect {|realization| realization.getImplementingClassifier }.sort
    super_interfaces = interface.getGeneral
    partition = partition_properties(interface, super_interfaces)
    # not certain that interface.getRedefinedInterface are the direct supers
    return unless builder.start_interface(interface, super_interfaces, realizers, interface.inbound_properties(package), partition  )
    builder.interface_cat_sequence(property_categories).each do |category|
      properties = partition[category]
      return unless builder.start_ifAttr_cat(category, properties)
      max_i = properties.size-1
      properties.each_with_index do |prop, i| 
        is_last = i==max_i
        if elements_are_included?(prop.type)
          builder.send("process_interface_#{category}", prop, prop.role_name, prop.mult, prop.type, interface, is_last)
        else
          Warnings << "Not processing #{interface.name} #{category} with name '#{prop.role_name}' because it has a type that is not part of the selected model"
        end
      end
      builder.end_ifAttr_cat(category)
    end
    builder.end_interface(interface)
  end
end


# Traverses a diagram instead of a model.
# =========== Why? ==================
# The idea is to make a custom view of the data
# in a separate diagram, that does not contain
# some classes, associations, and attributes that
# you do not want to include in a particular use case.
# =========== How it works ===========
# This gets all the presentation elements from the diagram
# (strictly speaking, from the diagram's presentation element).
# It then gets the model elements from each presentation element,
# and groups them by package.
# After that it pretty much works the same way as ModelDriven_Director,
# getting packages from the grouping described above (instead of the model),
# and filtering everything that gets processed, so that only model elements
# with presentation elements in the diagram get processed.
# =========== Known Bug ==============
# When you select an attribute and choose "Edit > Delete Symbols(s)"
# the attribute is hidden, but it still has a corresponding
# presentation element among recursive_child_views.
# I see that as a bug: if you delete the symbol of an association,
# it does NOT show up among recursive_child_views, why should an
# attribute behave differently?
# POSSIBLE FIX: There is probably some way to filter
# the results of recursive_child_views based on visibility (after all
# MagicDraw somehow knows it should render those hidden attributes)
class DiagramDriven_Director < Abstract_Director
  
  attr_accessor :diagramPrezElement
  attr_accessor :prez_elements
  
  # { element => [pres_element] } # Note that a single element can appear multiple times in the same diagram
  attr_accessor :prez_by_ele
  # { pkg => [pres_element] }
  attr_accessor :prez_by_pkg
  
  def packages; prez_by_pkg.keys; end
  def elements; prez_by_ele.keys; end
  def filter(collection); collection.to_a & elements; end
   
  def properties_for_classifier(classifier)
    # This is likely to be wrong so it was changed to something that is likely to be right but I'm not 100% sure.
    # properties = classifier.getOwnedAttribute.to_a
    properties = classifier.getDirectProperties.to_a
    remaining = properties & elements # attr_prez
    removed = properties - remaining
    puts "\nRemoved #{removed.size} properties" if 0!=removed.size
    remaining
  end
  
  # Argument is a diagram rather than a presentation element of a digram
  def process_diagram(diagram)
    self.prez_by_ele = Hash.new
    self.prez_by_pkg = Hash.new
    return unless builder.start_diagram(diagram)
    self.diagramPrezElement = $Proj.getDiagram(diagram) # method name is a misnomer
    self.prez_elements = diagramPrezElement.recursive_child_views
puts ">> Diagram has #{prez_elements.size.inspect} presentation elements (recursive_child_views)"
puts ">> Diagram has #{diagramPrezElement.getPresentationElements.size.inspect} presentation elements (getPresentationElements)" 
puts ">> Diagram has #{diagramPrezElement.getSelected.size.inspect} selected presentation elements"
    prez_elements.each {|pe| 
      ele = pe.getElement
      next unless ele # element is nil if it represents a hierarchical container that's not directly represented in the diagram (I've read that dependencies can be organized into groups that do not appear in the diagram)
      (prez_by_ele[ele] ||= Array.new) << pe 
      pkg = ele.owning_package # may be nil
      (prez_by_pkg[pkg] ||= Array.new) << pe
puts ">> Pkg #{pkg ? pkg.getName : 'nil'} contained a pe of class #{pe.class.name}"
      }
    packages.compact!
    analyze_graph(*packages) if finds_spanning_tree
    packages.each {|pkg| process_package(pkg) }
    builder.end_diagram(diagram)
  end
  
  # Child packages are not processed. That's because process_diagram iterates over all of the packages that have presentation elements in the diagram.
  # In other words, process_diagram is presuming flattened packages.
  def process_package(package, recursion_allowed)
    content_seq = builder.pkg_content_sequence(pkg_content(recursion_allowed))
    classifiers = content_seq.collect {|part| package.send(part)}.flatten
puts "== Before filtering: #{classifiers.size} in pkg #{package.getName}"
    classifiers = filter(classifiers)
puts "== After filtering: #{classifiers.size}"
    return unless builder.start_package(package, package.imported_packages, classifiers, package.roots.sort, content_seq)
    content_seq.each {|part| send "process_#{part}", package }
    builder.end_package(package)
  end
  
  # Methods dispatched by #process ============
  
  def process_primitives(package)
    contents = builder.sort_classifiers(package.primitives - package.enumerations)
    contents = filter(contents)
    return unless builder.start_section(:primitives, package, contents)
    contents = builder.sort_primitives inheritance_generalization(contents)
    contents.each {|prim| 
      super_prims = filter(prim.getGeneral)
      builder.make_prim(prim, super_prims) 
      }
    builder.end_section(:primitives)
  end
  
  def process_enumerations(package)
    contents = builder.sort_classifiers(package.enumerations)
    contents = filter(contents)
    return unless builder.start_section(:enumerations, package, contents)
    contents = builder.sort_enumerations inheritance_generalization(contents)
    contents.each {|enum| 
      super_enums = filter(enum.getGeneral)
      builder.make_enum(enum, super_enums, enum.literals) 
      }
    builder.end_section(:enumerations)
  end
  
  def process_classes(package)
    contents = builder.sort_classifiers(package.classes)
    contents = filter(contents)
    return unless builder.start_section(:classes, package, contents)
    contents = builder.sort_classes inheritance_super(contents)
    contents.each {|klass|
      super_classes = klass.getSuperClass
      super_classes = filter(super_classes)
      partition = partition_properties(klass, super_classes)
      itrfc_impl = filter(klass.__interfaces_implemented)
      next unless builder.start_class(klass, super_classes, itrfc_impl, klass.inbound_properties(package), partition )
      builder.class_cat_sequence(property_categories).each {|cat|
        attrs = partition[cat]
        next unless builder.start_clsAttr_cat(cat, attrs)
        max_i = attrs.size-1
        attrs.each_with_index {|attrib, i| 
          is_last = i==max_i
          builder.send("process_class_#{cat}", attrib, attrib.role_name, attrib.mult, attrib.type, klass, is_last)
          }
        builder.end_clsAttr_cat(cat)
        }
      builder.end_class(klass)
      }
    builder.end_section(:classes)
  end
  
  def process_interfaces(package)
    contents = builder.sort_classifiers(package.interfaces)
    contents = filter(contents)
    return unless builder.start_section(:interfaces, package, contents)
    contents = builder.sort_interfaces inheritance_generalization(contents)
    contents.each {|interface|
      realizations = interface.get_interfaceRealizationOfContract
      realizers = realizations.collect {|realization| realization.getImplementingClassifier }.sort
      realizers = filter(realizers)
      super_interfaces = interface.getGeneral
      super_interfaces = filter(super_interfaces)
      partition = partition_properties(interface, super_interfaces)
      # not certain that interface.getRedefinedInterface are the direct supers
      next unless builder.start_interface(interface, super_interfaces, realizers, interface.inbound_properties(package), partition  )
      builder.interface_cat_sequence(property_categories).each {|cat|
        attrs = partition[cat]
        next unless builder.start_ifAttr_cat(cat, attrs)
        max_i = attrs.size-1
        attrs.each_with_index {|attrib, i| 
          is_last = i==max_i
          builder.send("process_interface_#{cat}", attrib, attrib.role_name, attrib.mult, attrib.type, interface, is_last)
          }
        builder.end_ifAttr_cat(cat)
        }
      builder.end_interface(interface)
      }
    builder.end_section(:interfaces)
  end
  
end

